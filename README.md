# Pattern-Park Documentation

This Project contains the Documentation for
the [Pattern-Park application](https://gitlab.hochschule-stralsund.de/pattern-park/pattern-park-app/).

The Documentation contains descriptions for every pattern in the Pattern-Park.

At each release this project is bundled into an HTML site.

## Contents of the Documentation

Detailed descriptions for every pattern can be found at ``src/patterns/<patternname>/README.md``.

Each pattern description is linked in the [SUMMARY.md](src/SUMMARY.md).

Exact descriptions of the contents and technologies used in the Pattern-Park application can be found
at [src/README.md](src/README.md)

## Download

This documentation gets bundled into an HTML site when released. The compiled and bundled Documentation can be
downloaded from [GitLab](https://gitlab.hochschule-stralsund.de/pattern-park/pattern-park-app/-/releases) at
the ``other`` section.

## Compile

To manually compile this Project ``mdbook`` is required.

To build the documentation you can execute ``mdbook build``.

For further information on how to build a mdbook take a look at
the [CLI docs](http://rust-lang.github.io/mdBook/cli/build.html).

## Contributors

- course group "Softwareprojektorganisation" winter semester 2021 University Stralsund
- Aniketos Stamatios - aniketos.stamatios@protonmail.com
- Erik der Glückliche - contact [at] lui-studio.net
- NorthernSeaCharting - aasas@mail.de
- Soto - zauntormczaun@gmail.com
- GreenBird
- Kibarius der Erzmagier

## Libraries

- [mdbook](https://github.com/rust-lang/mdBook)
- [mdBook GitBook Theme from Luigi600](https://gitlab.com/Luigi600/mdbook-gitbook-theme)

## License

This application is licensed under the GNU General Public License, Version 2.

[GPLv2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html)