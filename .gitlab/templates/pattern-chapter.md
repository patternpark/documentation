# Patternname

Eine **kurze** Beschreibung des Patterns (wie im Tab "Das Muster").  
(Maximal 5 Sätze)

## Vorbereitungen

### Auswahl des Patterns
Eine kurze Erklärung, warum dieses Pattern ausgewählt wurde.

### (Zusätzliche Vorbereitungen)
Optional: Wenn besondere Vorbereitungen nötig waren, können diese hier erwähnt werden.  
(Z.B. ein zusätzliches Fahrgeschäft in die Parkkarte integrieren)

## (Limitierungen)
Optional: Gibt es Einschränkungen/Vereinfachungen die beachtet werden müssen?  
(Z.B. Beschränkung auf eine spezielle Implemntierung des Patterns oder Vereinfachung der genauen Funktionsweise des Patterns aufgrund der Anwendbarkeit aus das Beispiel)

## Beispiel aus dem Pattern-Park
Eine kurze Beschreibung des Beispiels und Erläuterung, wie damit das Pattern gelernt werden kann.

## Animation
Eine kurze Zusammenfassung des Inhaltes der Animation:  
Wie genau funktioniert das Pattern in Hinblick auf das Beispiel?

## UML-Puzzle
Welche Aufgabenstellung wurde gewählt und warum?

## Übung ohne UML
Welche Aufgabenstellung wurde gewählt und warum?  
Optional: Gibt es erwähnenswerte technische Details, die diese Übung von denen der anderen Gruppen unterscheiden?

## Übung mit UML
Welche Aufgabenstellung wurde gewählt und warum?  
Optional: Gibt es erwähnenswerte technische Details, die diese Übung von denen der anderen Gruppen unterscheiden?

## Verwendete Programme
Kurze Übersicht der verwendeten Programme als Liste
- Programm 1: Zur Erstellung von Grafiken
- Programm 2: Zur Erstellung von UML-Diagrammen
- Programm 3: .....
