# Strategy Pattern
Das Strategy-Pattern ermöglicht es einem, Instanzen desselben Objektes mit unterschiedlichen Verhaltensweisen zu erzeugen. 
Beispielsweise könnte eine Klasse "MatheOperation" so instanziiert werden, dass ihre Funktion "calculate(x,y)" 
entweder addiert, subtrahiert oder etwas anderes tut.   

## Vorbereitungen

### Auswahl des Patterns
Wir wählten das Pattern, da es den Eindruck machte, dass es an vielen Stellen anwendbar ist.
Außerdem bietet es eine Möglichkeit, den eigenen Code effektiver umzusetzen, 
da anstatt drei ähnlicher Klassen nur noch eine Kontext-Klasse benötigt wird, 
an die ein Objekt einer entsprechenden Strategie übergeben wird.

## Beispiel aus dem Pattern-Park
Aus den Attraktionen des PatternParks haben wir uns für den Eisverkäufer entschieden.
Ein Eisverkäufer kann mit Hilfe einer Verkaufsstrategie auf sich aufmerksam machen. 
Im Park gibt es mehrere Verkäufer, welche sich nur darin unterscheiden, welche Verkaufsstrategie sie anwenden.

In unserem Beispiel hat der Verkäufer die Wahl zwischen drei konkreten Verkaufsstrategien,
und zwar kann er entweder die Glocke des Wagens klingelt, die Angebote ausrufen, oder mit einem Werbeschild 
die Blicke auf sich ziehen. \
Dies soll als eine Echtwelt-Analogie dienen, um das allgemeine Konzept des Strategie-Patterns besser zu verständigen. 
Dabei dient der Verkäufer als Kontext-Klasse, welche ein Objekt einer konkreten Strategie-Klasse (z.B. "Glocken-Strategie")
entgegennimmt, wodurch das Verhalten des jeweiligen Verkäufer-Objektes bestimmt wird.
Die konkreten Strategie-Klassen implementieren alle das gleiche Interface "Strategie", welche eine Funktion beinhaltet, 
die überschrieben werden kann.
Der Verkäufer kann dann die Funktion "startWerben" aufrufen, wodurch die Strategie-Funktion "werben" aufgerufen wird.
"werben" zeigt dann, je nachdem, welche Strategie-Klasse an den Verkäufer übergeben wurde, ein anderes Verhalten.

## Animation
In der Animation zum Thema "Das Strategie-Pattern" wird das Pattern anhand des Eisverkäufers im Park erläutert.
(siehe Abschnitt "Beispiel aus dem Pattern-Park"). \
Es werden drei verschiedene Eisverkäufer aus dem Park vorgestellt. Jeder von Ihnen verfolgt eine andere Verkaufsstrategie:
Einer lockt Gäste mit einer Glocke, der zweite hält dazu ein Schild und der dritte ruft seine Angebote durch ein Megaphon.
Abgesehen von ihrer Strategie unterscheiden sie sich nicht. \
Anschließend wird das Szenario mit den drei Eisverkäufern anhand eines UML-Diagramms visualisiert, 
welches dem Muster des Strategie-Patterns entspricht. Es wird erklärt, dass alle drei Eisverkäufer Instanzen der gleichen Klasse "Verkäufer" sind.
Diese Instanzen nehmen dann ein Objekt einer konkreten Strategieklassen entgegen, welche das Interface "Verkaufsstrategie" implementieren.
So kann ein Verkäufer verschiedene Verhaltensweise zeigen, ohne dass die Klasse "Verkäufer" verändert werden muss.

## UML-Puzzle

## Übung ohne UML
Im allgemeinen wollten wir nicht zu weit, von bisherigen Übungen abweichen, 
darum entschieden wir uns für eine einfache Sortierung vorgegebener Elemente, per Drag and Drop. \
Die konkrete Aufgabenstellung bestand nun darin, dass wir als vorgegebenen Kontext einen Taschenrechner haben, 
welcher die Funktion "calculate(5,5);" aufruft. Wobei dem User 3 Ergebnisse in Form von Zahlen und 3 Strategien 
in Form von Operationen gegeben werden. Diese sollen dann in den richtigen Zusammenhang gebracht werden. 
Der Hintergrund dieser Übung besteht darin, es dem User zu verdeutlichen, dass sich bei dem Kontext 
und dem Aufruf seiner Funktion nichts ändert, sondern, dass schon bei dessen Instanziierung festgelegt wird, 
wie er sich verhält.

## Übung mit UML
Das Ziel dieser Übung war es, die UML-Umsetzung des Strategie-Patterns dem Nutzer näherzubringen. 
Durch das Bewältigen dieser Aufgabe soll der Nutzer ein besseres Gefühl dafür bekommen, 
wie die Programmstruktur eines Strategie-Patterns anhand einer gegebenen Situation umzusetzen ist. \
Nach dem Start der Übung sieht man einen Pool verschiedener Wortboxen. 
Darunter ist ein UML-Diagramm mit vordefinierter Struktur zu erkennen.
Die UML-Objekte haben noch keinen nennenswerten Inhalt, 
sondern bestehen jeweils nur aus zwei Platzhalterboxen für einen Klassen- und einen Funktionsnamen.
Die Aufgabe besteht nun darin, den einzelnen UML-Objekten die korrekten Wortboxen zuzuordnen. \
Beim Einsetzen der Boxen färbt sich der Rand des jeweiligen UML-Objektes. 
Dies dient dazu, anzuzeigen, ob die eigesetzte Lösung korrekt ist oder ob sie Verbesserungsbedarf hat. \



## Verwendete Programme
| Programm                    | Verwendungszweck |
| --------------------------- | ---------------- |
| IntelliJ                    | IDE für die Java-Entwicklung |
| VS Code                     | IDE für die Web-Entwicklung / Editor für die Dokumentation in Markdown|
| paint.net                   | Für die Bearbeitung von Bildern |
| Paint                       | Für die Bearbeitung von Bildern |
| Inkscape                    | Für die Erstellung einzelner Grafiken |
| Gimp                        | Für die Bearbeitung von Bildern |
