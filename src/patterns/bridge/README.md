# Brücke

Bei einer Brücke hat eine Klasse, die die Rolle der Abstraktion spielt, eine Aggregation mit einer anderen Klasse, die die Rolle der Implementation spielt.
Statt für jede Besonderheit, die hier die Unterklassen der Implementation haben würde, eine neue Unterklasse für die Abstraktion zu erstellen, werden die Objekte aus der Hirarchie der Implementation von den Klassen der Abstraktion referenziert, sodass eine große Anzahl an Klassen vorgebeugt wird.

## Vorbereitungen

### Auswahl des Patterns
Das Pattern wurde gewählt, weil es nicht zu kompliziert ist, es anhand des Pattern-Parks zu erklären und passende Übungen zu finden.

## Beispiel aus dem Pattern-Park
Das grundlegende Beispiel für die Brücke aus dem Pattern sind Pflanzen als Abstraktion und Farben als Implementation.
Man kann das Pattern lernen, indem man durch dieses Beispiel versteht, warum es vorteilhaft ist, dass nicht für jede unterschiedlich farbige Pflanze eine neue Klasse erstellt wird, sondern dass die Pflanze die Farbe nur referenziert.

## Animation
In der Animation wird anhand des Beispiels der Pfanzen und Farben erklärt, was der Vorteil einer Brücke ist.
Hier ist die Pflanze die Abstraktion und die Farbe ist die Implementaion. Baum und Busch sind die Unterklassen von Pflanze und Gelb, Braun und Grün sind die Unterklassen von Farbe.

## UML-Puzzle
(Welche Aufgabenstellung? Warum?)
Das Ziel der Aufgabe ist es, Unterklassen für das Interface Farbe und der abstrakten Klasse Baum zu erstellen, um verschiedene Baumarten mit verschiedenen Farbkombinationen verbinden zu können.
Ziel der Aufgabe war es, das Wissen über Brücken anzuwenden und zu stärken und gleichzeitig die Aufgabe simpel zu halten.

## Übung ohne UML
In dieser Aufgabe muss bestimmt werden, welche der Objekte die Abstraktion und welche die Implementation sind. Sie dient der Verfestigung des Wisssen über das Entwurfsmuster Brücke, durch ergänzung der Typen von den gegebenen Objekten.

## Übung mit UML
Die Aufgabe besteht daraus, die leeren Felder in dem Klassendiagramm einer Brücke mit den gegebenen Klassen zu ergänzen.
Bei dieser Aufgabe kann man verfestigen, wie eine Brücke in der Regel mindestens aussieht, indem man die Beziehungen zwischen den gegebenen Klassen erkennt.

## Verwendete Programme
- Visual Studio Code: Programmieren
- paint.net: Grafikdesign
- GIMP: Grafikdesign
- Powerpoint: Animation ohne Ton
- Audacity: Tonbearbeitung für Animation
- Lightworks: Schneiden und Zusammenführen von Animation und Ton.
- https://www.diagrammeditor.de/ für das UML-Diagramm.
- Google Übersetzer: Vertonung des Skripts für die Animation.