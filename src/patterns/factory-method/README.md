# Fabrik-Methode

Die Fabrikmethode ist ein Muster, um die Erzeugung von Objekten bestimmter Klassen zu trennen / zu organisieren
und so eine lose Kopplung zu erreichen. Es gehört zu den klassen-basierten Entwurfsmustern.

## Vorbereitung

### Auswahl des Musters
Das Muster wurde gewählt, weil es eines der komplizierteren Entwurfsmuster war, dabei allerdings immer noch nicht überbordend schwer anmutete. Außerdem erschien uns das Muster recht praxisnah und kein rein theoretisches Modell zu sein, welches in der Entwicklung Anwendung finden könnte.

### Zusätzliche Vorbereitung
Um das Muster im Pattern Park umsetzen zu können, mussten wir ein zusätzliches “Element” in die Parkkarte integrieren. Dieses Element war die graphische Repräsentation eines Imbiss, welcher dem gewählten Beispiel für die Beschreibung des Musters entspricht.

## Beispiel aus dem Pattern Park
Um die Einzelheiten des Musters anschaulich erläutern zu können, wählte die Gruppe als Beispiel eine “Imbiss-Situation” aus. Das heißt, dass für den Erzeuger des Musters bildlich der Imbiss verwendet wurde, welcher dann Nahrung / Essen als Produkt für die Besucher des Parks erzeugt. Da es mehrere Essensrichtungen gab, mussten dementsprechend auch mehrere Imbisse als Erzeuger existieren, was die Analogie für das Muster abrundete.

Wie schon eingangs bei der Beschreibung des Musters angeführt, dient das Muster dazu, das Erzeugen von verschiedenen, ähnlichen Produkten zu organisieren und eine lose Kopplung zu erreichen. Die Imbisse dienen dabei als gute Repräsentationen der Erzeuger, weil alle Imbisse in irgendeiner Form Essen / Nahrung erzeugen und dabei viele gleiche Operationen und Verhaltensweisen auf sich vereinen. Dazu gehören zum Beispiel Bestellungen annehmen oder Angebote machen und so weiter und so fort. Sie unterscheiden sich jedoch darin, welche Art Essen sie produzieren.
Damit dient das spezifische Essen als ein gutes Produkt, da es Teil einer gleichen Grundart (Essen) ist, sich aber in seinen spezifischen Details unterscheidet (chinesisches Essen ist zwar Essen, aber anders als italienisches usw.).
In unseren Augen stellt dies eine gute bildliche Repräsentation des Musters dar.

## Die Animation
Im Video geht es darum, dass der Pattern Park sein Essens-Angebot ausweiten möchte, dafür aber Verkaufsstellen braucht, die das Essen zubereiten können. Da kommen die Imbisse ins Spiel, die einen Bauplan für die gebrauchten Erzeuger stellen. Mit dieser Hilfe kann das neue Essensangebot dann leicht umgesetzt werden.
Das Muster funktioniert in dem Beispiel ähnlich, wie das zu dem Zeitpunkt bereits in den restlichen Übungen der Fall war: Die Imbisse repräsentieren die Erzeuger, die als “Bauanleitung” für die Gerichte / das Essen (die Produkte) dienen. Dadurch ist es im Beispiel möglich, effizient und schnell die benötigten Essensrichtungen anzubieten. Das entspricht dem Vorteil, der allgemein aus den Erzeugern gezogen werden soll (dass es so möglich ist, leicht neue Produkte in den bestehenden Code zu integrieren und mit diesen zu arbeiten).

## UML Puzzle
Bei den Puzzles der Muster, die vor Beginn der Arbeiten vorhanden waren, war wenig Diversität vorzufinden. Generell beschränkte sich die Aufgabenstellung auf Zuordnungsaufgaben, bei denen die fehlenden Einzelteile ergänzt und die gesuchten Verbinden eingetragen werden mussten. Diesem Beispiel folgte auch die Umsetzung der Gruppe. Damit war bei der Wahl der Aufgabenstellung eine klare Vorlage / Richtung vorgegeben, von der nicht abgewichen wurde. Lediglich die Einzelheiten (also zum Beispiel welche Objekte und Details eingetragen werden müssten) unterschieden sich. Sie wurden so gewählt, dass sowohl die Idee des vorhergehenden Beispiels beibehalten wurde, als auch eine interessante Aufgabe entstehen würde.

## Übung ohne UML
Als Aufgabenstellung setzte sich aus zwei Bestandteilen zusammen: Teil 1 beschrieb eine Zuordnungsaufgabe, bei der die Schüler für eine Reihe von Elementen bestimmen sollten, ob es sich um Erzeuger oder Produkte handelt. Als eine zusätzliche Komplikation wurden Beispiele eingefügt, die weder das eine, noch das andere waren. Dies sollte sicherstellen, dass die Schüler bei der Bearbeitung überlegen würden, bevor sie die Zuordnung vornehmen.
Die Aufgabenstellung wurde gewählt, um den Schülern die Aufteilung zwischen Erzeugern und Produkten klarzumachen und ein Gefühl für das Konzept hinter den Klassen zu gewinnen.


Die zweite Aufgabenstellung beschrieb zwar auch eine Zuordnungsaufgabe, allerdings sollten die Schüler hier die Erzeuger den Produkten zuordnen.
Das sollte dazu dienen, den Schülern die Beziehung zwischen den Produkten und Erzeugern vor Augen zu führen.

### Technische Details
Damit die Aufgabe umgesetzt werden konnte, wurden Angulars vorgefertigte Droplist-Funktionalitäten eingebunden und in die Elemente der Aufgabe integriert. Außerdem wurde die Leader-Line-Bibliothek eingebunden, um Verbindungen zwischen den Elementen der zweiten Aufgabe zeichnen zu können.

## Übung mit UML
Für diese Übung wurde eine Aufgabe mit einem Sequenzdiagramm erstellt. Dafür wurden eine Reihe Vorfälle beschrieben, die mittels des Sequenzdiagramms dargestellt werden sollten. Diese Aufgabe sollte die Nutzung des Musters im “Alltag” demonstrieren und ein Gefühl für die Anwendung erzeugen. Außerdem würde die direkte Interaktion der Schüler mit dem Klassendiagramm des Musters, den Umgang mit dem Werkzeug und die Verbindung zu Sequenzdiagrammen lehren.

### Technische Details
Für die Umsetzung dieser Aufgabe wurde die Bibliothek “mermaidjs” für die Erzeugung der visuellen Repräsentation der UML-Diagramme eingebunden. Für eine bessere Nutzung der Klasse wurden Wrapper und Generatoren geschrieben, die die Arbeit mit der Bibliothek vereinheitlichen und vereinfachen sollen.

## Verwendete Programme

| Programm                    | Verwendungszweck |
| --------------------------- | ---------------- |
| IntelliJ                    | IDE für die Java-Entwicklung |
| WebStorm                    | IDE für die Web-Entwicklung |
| VS Code                     | IDE für die Web-Entwicklung |
| Violet UML Editor           | Zur Erstellung von UML-Diagrammen |
| paint.net                   | Für die Bearbeitung von Bildern |
| Gimp                        | Für die Bearbeitung von Bildern |
| Inkscape                    | Zur Bearbeitung/Erstellung von Vektorgrafiken |
| JPEXS Free Flash Decompiler | Zum Entpacken der Vektorgrafiken aus dem Pattern Park Flash Dateien (notwendig für die Animation und den Zoom in die Karte (in hochauflösender Qualität)) |
| Java Decompiler             | Zum Anschauen der Ressourcen in der Jar-Datei, die veröffentlicht worden ist |








