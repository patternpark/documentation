# Adapter

Das Adapter-Pattern ermöglicht die Kommunikation von Klassen mit inkompatiblen Schnittstellen. 
Es gehört zu der Familie der objektbasierten Strukturmuster.

## Vorbereitungen

### Auswahl des Patterns
Das Pattern wurde primär anhand des Beispiels für das Pattern im Pattern Park ausgewählt.
Mehrere GoF-Pattern waren in der Vorauswahl, das Adapter-Pattern bot dann die beste Möglichkeit für ein verständliches Beispiel.

Zudem ist es ein für Schüler gut lernbares Pattern, das auch in der Realität der Softwareentwicklung oft Verwendung findet.

## Limitierungen
Bezüglich der Realisierung des Patterns in Java-Code und UML wurde sich dazu entscheiden, lediglich den Objektadapter zu behandeln.  

Der Klassenadapter wird also nicht behandelt, da dies:  
a) den Umfang im Rahmen einer Lernsoftware für Schüler sprengen würde und   
b) der Klassenadapter nur in Programmiersprachen, die Mehrfachvererbung und private Vererbung erlauben, umgesetzt werden kann

## Beispiel aus dem Pattern-Park
Es wurde folgendes Beispiel gewählt:  
"Die Bergbahn ist schon länger in Benutzung. Jetzt mussten einige der Waggons ausgetauscht werden. Die neuen Waggons haben eine andere Kupplung als die alten Waggons. Deshalb wird ein Adapter eingesetzt, um die Waggons zu verbinden."

Die Funktionsweise des Adapter-Patterns wird an diesem Beispiel gut illustriert, da die Bestandteile des Beispiels direkt auf die Softwareentwicklung übertragbar sind.
So repräsentiert ein alter Waggon eine bestehende Klasse und ein neuer Waggon eine zweite bestehende Klasse mit anderer Schnittstelle.
Analog zu Klassen ist das Beispiel auch auf vollständige Softwaresysteme, etc. übertragbar.

Die Kupplungen repräsentieren dabei sehr anschaulich die unterschiedlichen Schnittstellen.
So kann die Funktion des Adapters als "Übersetzer" zwischen den Schnittstellen schnell erklärt werden.

## Animation
Im Animationsvideo wird zunächst das Beispiel vorgestellt und noch einmal hervorgehoben, warum ein Adapter benötigt wird.
Dann folgt ein kurzer Exkurs zu Objektadapter und Klassenadapter mit dem Hinweis, das im Rahmen des Pattern-Parks nur der Objektadapter behandelt wird.

Im Anschluss wird die Übertragung des Beispiels der Waggons auf das generelle Pattern in der Softwareentwicklung erläutert.
Dafür werden die Bestandteile des Beispiels (Neuer Waggon, Alter Waggon, Adapter) den passenden Klassen im Klassendiagramm zugeordnet.
So wird eine Verknüpfung zwischen dem Einstiegsbeispiel aus dem Pattern Park und den Übungen (UML-Puzzle, Übung mit UML) hergestellt.

## UML-Puzzle
Bei der Aufgabenstellung des UML-Puzzle wurde sich an den Aufgabenstellungen der bereits vorhandenen Puzzle anderer Patterns orientiert.
Die Aufgabe lautet daher, ein allgemeines Klassendiagramm des Adapter-Patterns auf das Beispiel der Waggons anzupassen.
Dafür müssen die Namen der Klassen angepasst und die richtigen Verknüpfungen zwischen den Klassen (Vererbung/Assoziation/etc.) hergestellt werden.

So soll das Verständnis der Klassenstruktur des Patterns erhöht werden.

## Übung ohne UML
Bei der Übung ohne UML geht es darum, die Bausteine des Adapter-Patterns ihrer Rolle im richtigen Pattern zuzuordnen.
Dafür werden die Bausteine aller Patterns durchmischt angezeigt und können dann per Drag and Drop an die richtige Stelle im richtigen Pattern gezogen werden.

Die Schüler sollen so die Funktionsweise und die Rollen des Patterns an weiteren Echtwelt-Beispielen üben und auf ein erstes Beispiel aus der Softwareentwicklung anwenden.

Um in Zukunft neue Beispiele hinzufügen zu können, wurde eine Komponente für ein einzelnes Beispiel erstellt.
Andere Teile der Übung, wie z.B. die Validierung des Ergebnisses funktionieren bei Erweiterung out of the box.

## Übung mit UML
Bei der Übung mit UML sollen Fehler im bestehenden Sequenzdiagramm behoben werden.
Dafür wird ein Sequenzdiagramm mit drei Fehlern gerendert.
Klickt man auf ein Objekt oder eine Nachricht, öffnet sich eine Auswahl mit drei Fehlermöglichkeiten.
Wird der richtige Fehler ausgewählt, korrigiert sich das Diagramm automatisch.

So soll das Verständnis für den Ablauf der Kommunikation zwischen den Teilnehmern des Patterns erhöht werden.

Da Mermaid.js keine Funktionen zur Manipulation des Diagramms oder für Click-Events bietet, wurden die HTML-Elemente manipuliert.
Sollten weitere, ähnliche Übungen eingebunden werden, sollte hier ein Wrapper Einzug halten.

## Verwendete Programme
| Programm                    | Verwendungszweck |
| --------------------------- | ---------------- |
| IntelliJ                    | IDE für die Java-Entwicklung |
| Visual Studio Code          | IDE für die Web-Entwicklung |
| Visual Paradigm Online      | Zur Erstellung von UML-Diagrammen |
| paint.net                   | Für die Bearbeitung von Bildern |
| Photoshop Elements 10       | Für die Bearbeitung von Bildern |
| Power Point                 | Zur Animation des Videos |
| Open Broadcaster Software   | Zur Aufnahme des Animationsvideos |

