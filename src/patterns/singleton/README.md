# Singleton

Das Singleton-Pattern gehört zu den objektbasierten Erzeugungsmustern.
<br>
Es stellt sicher, dass nur ein Objekt einer Klasse erzeugt werden kann.
<br>
Dieses Objekt wird durch einen globalen Zugriffspunkt bereitgestellt.
<br>
Ein Client kann nur über diesen Zugriffspunkt auf eine Instanz der Klasse zugreifen.

## Vorbereitung

### Auswahl des Musters
Das Muster wurde gewählt, weil uns das Muster schon bekannt war und
es trotz des einfachen Aufbaus ein sehr mächtiges Pattern ist.


## Beispiel aus dem Pattern Park
Als Beispiel aus dem Pattern Park haben wir die Kassen am Eingang gewählt.
<br>
Im Park existiert nur ein Kassengebäude mit zwei Kassen.
<br>
Sie liefern ein gutes Beispiel für Statistikerhebung mit Singletons.

## Die Animation
In der Animation zum Singleton geht es darum, dass der Pattern Park eine 
Statistik über Besucherzahlen erheben möchte.
<br>
Damit am Ende des Tages die Zähler nicht zusammengerechnet werden müssen und <br>
damit die Statistik ohne Mehraufwand ablesbar ist, wird hierfür das Singleton-Pattern eingesetzt.

## UML Puzzle
Im UML Puzzle haben wir uns anhand der bereits vorhandenen Übungen orientiert.
<br>
Wir haben also ein Problem geschaffen, für dass das Singleton-Pattern eine gute Lösung bietet.


Es gibt 3 Klassen, welche jeweils alle auf eine Instanz einer anderen Klasse zugreifen müssen.
<br>
Damit diese allerdings alle auf dieselbe Instanz zugreifen,
müssen noch wichtige Zugriffsmethoden und Verbindungen ergänzt werden.

## Übung ohne UML
Für diese Übung wurde eine Zuordnungsaufgabe erstellt in der, der Nutzer entscheiden muss,
ob das Singleton-Pattern für das gegebene Beispiel geeignet wäre.
<br>
Wird erst überprüft, wenn der entsprechende Knopf gedrückt wurde.


### Technische Details
Die Übung wurde durch Rendern von Arrays in ein Listenelement umgesetzt.
Auf diesen Listenelementen befinden sich Click-listener, durch welche sich die Elemente 
auswählen und in andere Listen verschieben lassen.
Durch den Klick, auf den *Lösung überprüfen* Button, werden dann die Listen mit einer Musterlösung verglichen.


## Verwendete Programme

| Programm                    | Verwendungszweck |
| --------------------------- | ---------------- |
| IntelliJ IDEA Ultimate                    | IDE für die Java- und Web-Entwicklung |
| VS Code                     | IDE für die Web-Entwicklung |
| [draw.io](https://draw.io)           | Zur Erstellung von UML-Diagrammen |
| paint.net                   | Für die Bearbeitung von Bildern |
| Gimp                        | Für die Bearbeitung von Bildern |
| Microsoft PowerPoint Online | Für die Erstellung der Animation |
| Open Broadcaster Studio | Für die Aufnahme der Animation |
| Kdenlive | Für die Nachbereitung der Animation |
| [freetts.com](https://freetts.com) | Für die Text-To-Speech Audiospur für die Animation |
