# Pattern Park

## Vorbemerkungen

Diese Dokumentation behandelt lediglich die Änderungen und neuen Features, die im Rahmen der Veranstaltung _Software Engineering II_ im Wintersemester 2021/2022 an der Hochschule Stralsund entstanden sind.

## Implementierung der Übungen

### Limitierungen durch die bestehende Codebase

Die bestehenden Übungen wurden in _Flash_ umgesetzt.  
Flash ist allerdings seit 2020 obsolet und sollte daher auf keinen Fall mehr zur Implementierung neuer Features genutzt werden. Da der Pattern Park in Java Swing umgsetzt wurde und Java Swing, wie auch JavaFX veraltet ist (da es keinen offiziellen Support von Oracle mehr gibt), erschien die Einarbeitung in ein solches Java Frontend-Framework eher kontraproduktiv. Durch die Verwendung von Flash durch das vorherige Entwicklerteam ist davon auszugehen, dass Java Swing keine vergleichbaren Bibliotheken hatte, die es zum Beispiel ermöglichen würden, Elemente in andere Elemente via _Drag & Drop_ zu verschieben. Die Wartbarkeit der Software hätte sich dadurch noch weiter verschlechtert und vieles hätte man vermutlich selbst implementieren müssen.

### Angular

Als Alternative zu Flash wurde sich für _[Angular](https://angular.io/)_ entschieden.  
Folgende Gründe sprachen dafür:

- Es handelt sich um ein Frontend-Webapplikationsframework. Somit ist es plattformübergreifend einsetzbar (wie auch Java und der Pattern Park, welcher Windows, Linux und MacOS unterstützt).
- Aus der Lehrveranstaltung _Web Engineering I_ waren JavaScript, HTML und CSS bereits allen Entwicklern bekannt. Somit ist eine Einarbeitung in Angular einfacher, als in anderen Frontend-Frameworks, da in der Theorie normales JavaScript verwendet werden kann.
- Bei Angular ist - anders als z.B. bei _React_ kein Komponenten-Wrapper notwendig. Daher gibt es keine Einschränkungen an Browser Bibliotheken von npm.
- Angular bekommt regelmäßige Updates und ist gut geeignet für große Projekte.

### NPM

Der _Node package manager_ und die dazugehörige npm Registry bieten eine enorme Anzahl an Bibliotheken, die sich ohne großen Aufwand einbinden lassen. Dadurch verkürzt sich die Entwicklungszeit, da sich nur noch in eine Bibliothek eingearbeitet und nicht mehr alles selbst implementiert werden muss.

So konnten u.a. _Mermaid_ zum Erstellen von Klassen- und Sequenzdiagrammen, _leader-line_ zum Zeichnen von Linien für die Übungen und weitere Bibliotheken in das Projekt integriert werden.

Für die Weiterentwicklung des Projekts erlaubt npm die Einbindung von weiteren Bibliotheken in Sekundenschnelle.

### Electron

Ursprünglich sollten die Angular-Übungen in einem Browserfenster aus der Java-Anwendung gestartet werden.  
Allerdings gab es nur wenige kostenlose Möglichkeiten zur Einbindung eines Browsers. Darunter erschien die _WebView_ aus _JavaFX_ am einfachsten.
Diese unterstützt allerdings keine JavaScript-Version, in die Angular kompiliert werden kann. Eine weitere Alternative war die Einbindung von Chromium als Bibliothek. Allerdings musste dafür der Chromium-Browser auf dem Ziel-System selbst kompiliert werden. Somit wäre eine lauffähige Anwendung ohne Installer ausgeschlossen.

Daher wurde sich entschieden, die Angular-Übungen in eine _[Electron](https://www.electronjs.org/)_ Anwendung auszulagern. Dadurch wird der Browser selbst mitgeliefert und die Version des Browsers ist vorgegeben. Durch dieses Vorgehen reduzieren sich Fehler bei der Verwendung, da der Endanwender keinen veralteten Browser oder ähnliches verwenden kann.
Die Anwendung kann mit Parametern gestartet werden, um die zu startende Übung zu spezifizieren.

## Abspielen der Videos

Auch die bestehenden Animationsvideos basierten auf Flash.
Für die neuen Videos musste also eine neue Lösung gefunden werden.

Es wurde sich auf ein MP4-Format geeinigt, da dieses die beste Kompatibilität zu unterschiedlichen Systemen bietet.

Zum Abspielen wurde zunächst eine Java-Bibliothek gesucht.
Es zeigte sich allerdings schnell, dass lediglich veraltete Bibliotheken zur Verfügung standen.

Daher wurde sich dazu entschieden, das Video im Angular-Projekt über HTML5-Video abzuspielen. Dafür wurde eine Video-Player-Komponente entwickelt und die Möglichkeit, das Video per Aufruf der Electron-App mit Parametern zu starten, implementiert.

## Weitere Bemerkungen zur bestehenden Codebase

Wie bereits im Abschnitt zu Flash bemerkt, sollten veraltete Sprachen und Technologien nicht mehr genutzt werden.

Daher sollte in Zukunft darüber nachgedacht werden, den auf _Java Swing_ basierenden Code zu überarbeiten.
Dieses wurde bereits 2014 durch den neuen De-facto-Standard JavaFX abgelöst.  
Aus heutiger Sicht scheint allerdings selbst JavaFX für dieses Projekt nicht mehr angemessen. So ist die Anzahl an Bibliotheken überschaubar und die Einarbeitung in JavaFX schwierig.

Als Alternative könnte - insbesondere um eine einfache Benutzbarkeit für den Einsatz in Schulen zu erlauben - eine Web-App in Frage kommen.
Falls eine Desktopanwendung benötigt wird, könnte diese vollständig mit Electron umgesetzt werden.
