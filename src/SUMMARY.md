# Zusammenfassung

- [Einleitung](README.md) <!-- deckblatt -->
  - [Pattern - Fabrikmethode](patterns/factory-method/README.md)
  - [Pattern - Adapter](patterns/adapter/README.md)
  - [Pattern - Singleton](patterns/singleton/README.md)
  - [Pattern - Brücke](patterns/bridge/README.md)
  - [Pattern - Strategie](patterns/strategy/README.md)
